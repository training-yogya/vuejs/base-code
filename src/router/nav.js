export const sidebar = [
  {
    icon: "mdi-monitor",
    name: "Dashboard",
    to: "/dashboard",
    color: "#000000"
  },
  {
    icon: "mdi-account-multiple",
    name: "Master",
    to: "/master",
    color: "#000000"
  },
  {
    icon: "mdi-account",
    name: "Karyawan",
    to: "/karyawan",
    color: "#000000"
  },
  {
    icon: "mdi-airplane",
    name: "Barang",
    to: "/barang",
    color: "#000000"
  },
  {
    icon: "mdi-archive",
    name: "Master Data",
    to: "/master-data",
    color: "#000000"
  },
  {
    icon: "mdi-cube",
    name: "Inventory",
    to: "/inventory",
    color: "#000000"
  },
  {
    icon: "mdi-barcode-scan",
    name: "Barcode Scanner",
    to: "/barcode",
    color: "#000000"
  },
  {
    icon: "mdi-barcode",
    name: "Barcode Generator",
    to: "/barcode-generator",
    color: "#000000"
  },
  {
    icon: "mdi-checkbox-marked-circle-outline",
    name: "Check List",
    to: "/check-list",
    color: "#000000"
  },
  {
    icon: "mdi-file-document",
    name: "Input FIle",
    to: "/input-file",
    color: "#000000"
  }
];
