import Vue from "vue";
import VueRouter from "vue-router";
import Cookies from "js-cookie";
import Clean from "@/views/layouts/CleanLayout.vue";
import Home from "@/views/layouts/HomeLayout.vue";

Vue.use(VueRouter);
const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Root",
      component: Clean,
      redirect: "login",
      children: [
        {
          path: "login",
          name: "login",
          component: () => import("@/views/pages/login"),
          meta: {
            guest: true
          }
        },
        {
          path: "",
          name: "Dashboard",
          redirect: "dashboard",
          component: Home,
          children: [
            {
              path: "dashboard",
              name: "Home Dashboard",
              component: () => import("@/views/pages/dashboard/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "master",
              name: "Master",
              component: () => import("@/views/pages/master/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "karyawan",
              name: "karyawan",
              component: () => import("@/views/pages/karyawan"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "barang",
              name: "Barang",
              component: () => import("@/views/pages/masterBarang/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "barcode",
              name: "Barcode",
              component: () => import("@/views/pages/barcode/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "check-list",
              name: "Check List",
              component: () => import("@/views/pages/checkList/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "input-file",
              name: "Input FIle",
              component: () => import("@/views/pages/inputFile/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "barcode-generator",
              name: "Barcode Generator",
              component: () =>
                import("@/views/pages/barcodeGenerator/index.vue"),
              meta: {
                requireAuth: true
              }
            },
            {
              path: "master-data",
              name: "Master Data",
              component: () => import("@/views/pages/masterData"),
              redirect: "/master-data/list-data",
              meta: {
                requireAuth: true
              },
              children: [
                {
                  path: "list-data",
                  name: "List Data",
                  component: () =>
                    import("@/views/pages/masterData/view/table"),
                  meta: {
                    requireAuth: true
                  }
                },
                {
                  path: "add",
                  name: "Add Data",
                  component: () =>
                    import("@/views/pages/masterData/view/formAdd"),
                  meta: {
                    requireAuth: true
                  }
                },
                {
                  path: "edit/:id",
                  name: "Edit Data",
                  component: () =>
                    import("@/views/pages/masterData/view/formEdit"),
                  meta: {
                    requireAuth: true
                  }
                }
              ]
            },
            // inventory
            {
              path: "inventory",
              name: "Inventory",
              component: () => import("@/views/pages/inventory"),
              redirect: "/inventory/list-inventory",
              meta: {
                requireAuth: true
              },
              children: [
                {
                  path: "list-inventory",
                  name: "List Inventory",
                  component: () => import("@/views/pages/inventory/view/table"),
                  meta: {
                    requireAuth: true
                  }
                },
                {
                  path: "add",
                  name: "Add Inventory",
                  component: () =>
                    import("@/views/pages/inventory/view/formAdd"),
                  meta: {
                    requireAuth: true
                  }
                },
                {
                  path: "edit/:id",
                  name: "Edit Inventory",
                  component: () =>
                    import("@/views/pages/inventory/view/formEdit"),
                  meta: {
                    requireAuth: true
                  }
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: "*",
      name: "Not Found",
      component: () => import("@/views/Error/NotFound.vue")
    }
  ]
});
router.beforeEach((to, from, next) => {
  const token = Cookies.get("token");
  if (to.matched.some(record => record.meta.requireAuth)) {
    if (token === null || token === undefined) {
      next("/login");
    } else {
      next();
    }
  } else if (to.matched.some(record => record.meta.guest)) {
    if (token === null || token === undefined) {
      next();
    } else {
      next("/dashboard");
    }
  }
});
const DEFAULT_TITLE = "MyTraining";

router.afterEach(to => {
  Vue.nextTick(() => {
    document.title =
      process.env.VUE_APP_TITLE + " - " + to.name || DEFAULT_TITLE;
  });
});
export default router;
