const mutations = {
  SET_RESDATA(state, data) {
    state.dataResponse = data;
  },
  SET_IDDATA(state, data) {
    state.idData = data;
  },
  SET_DATA(state, data) {
    state.dataMaster = data;
  },
  SET_RESGRADE(state, data) {
    state.listGrade = data;
  }
};
export default mutations;
