import api from "../../../services/api";
const actions = {
  getData: ({ commit }) => {
    return new Promise((resolve, reject) => {
      api.get("/training").then(
        response => {
          const res = response.data;
          resolve(response);
          if (res.status) {
            commit("SET_RESDATA", res.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getDataById: ({ commit, state }) => {
    return new Promise((resolve, reject) => {
      api.get("/training/" + state.idData).then(
        response => {
          const res = response.data;
          resolve(response);
          if (res.status) {
            commit("SET_RESDATABYID", res.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
  },
  insertData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.post("/training", state.tempData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  editData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.put("/training/" + state.idData, state.tempData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deteleData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.delete("/training/" + state.idData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

export default actions;
