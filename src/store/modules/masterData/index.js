import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
import state from "./state";

const masterData = {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};

export default masterData;
