const mutations = {
  SET_RESDATA(state, payload) {
    state.resMasterData = payload;
  },
  SET_TEMPDATA(state, payload) {
    state.tempData = payload;
  },
  SET_IDDATA(state, payload) {
    state.idData = payload;
  },
  SET_RESDATABYID(state, payload) {
    state.resDataById = payload;
  }
};
export default mutations;
