import api from "../../../services/api";
const actions = {
  getData: ({ commit }) => {
    return new Promise((resolve, reject) => {
      api.get("/training/array").then(
        response => {
          const res = response.data;
          resolve(response);
          if (res.status) {
            commit("SET_RESDATA", res.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getProduct: ({ commit, state }) => {
    return new Promise((resolve, reject) => {
      api
        .get("/training/product", {
          params: {
            search: state.search
          }
        })
        .then(
          response => {
            const res = response.data;
            resolve(response);
            if (res.status) {
              commit("SET_LISTPRODUCT", res.data);
            }
          },
          error => {
            reject(error);
          }
        );
    });
  },
  insertData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.post("/training/array", state.tempData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  getDataById: ({ commit, state }) => {
    return new Promise((resolve, reject) => {
      api.get("/training/array/" + state.idData).then(
        response => {
          const res = response.data;
          resolve(response);
          if (res.status) {
            commit("SET_RESDATABYID", res.data);
          }
        },
        error => {
          reject(error);
        }
      );
    });
  },
  editData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.put("/training/array/" + state.idData, state.tempData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  },
  deteleData: ({ state }) => {
    return new Promise((resolve, reject) => {
      api.delete("/training/array/" + state.idData).then(
        response => {
          resolve(response);
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

export default actions;
