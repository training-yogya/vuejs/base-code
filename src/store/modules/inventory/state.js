const state = () => ({
  resMasterData: [],
  tempData: [],
  idData: null,
  resDataById: [],
  listProduct: [],
  search: null
});

export default state;
