const mutations = {
  SET_RESDATA(state, payload) {
    state.resMasterData = payload;
  },
  SET_TEMPDATA(state, payload) {
    state.tempData = payload;
  },
  SET_IDDATA(state, payload) {
    state.idData = payload;
  },
  SET_RESDATABYID(state, payload) {
    state.resDataById = payload;
  },
  SET_LISTPRODUCT(state, payload) {
    state.listProduct = payload;
  },
  SET_SEARCH(state, payload) {
    state.search = payload;
  }
};
export default mutations;
