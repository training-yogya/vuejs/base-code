import actions from "./actions";
import mutations from "./mutations";
import state from "./state";

const inventory = {
  namespaced: true,
  state,
  actions,
  mutations
};

export default inventory;
