import api from "../../../services/api";
import Cookie from "js-cookie";

const actions = {
  doLogin: ({ state }) => {
    return new Promise((resolve, reject) => {
      console.log(state.dataLogin);
      api.post("/user/auth/login", state.dataLogin).then(
        response => {
          Cookie.set("data_user", JSON.stringify(response.data), {
            expires: 1
          });
          api.post("/token/request", state.dataLogin).then(
            response => {
              resolve(response);
            },
            error => {
              reject(error);
            }
          );
        },
        error => {
          reject(error);
        }
      );
    });
  }
};

export default actions;
