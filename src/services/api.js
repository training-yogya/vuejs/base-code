import axios from "axios";
import store from "../store";

const api = axios.create({
  baseURL: process.env.VUE_APP_BASE_API_URL
});

api.interceptors.request.use(
  config => {
    const clientToken = store.getters.clientToken;

    const customConfig = config;

    if (clientToken) {
      customConfig.headers.ip = "111.11.1";
      customConfig.headers.origin = "yogyagroup.com";
      customConfig.headers.source = "sails-api";
      customConfig.headers.Authorization = `Bearer ${clientToken}`;
    }

    return customConfig;
  },
  error => {
    return Promise.reject(error);
  }
);

export default api;
